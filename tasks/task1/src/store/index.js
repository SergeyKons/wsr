import Vue from "vue";
import Vuex from "vuex";
import VuexPersist from 'vuex-persist';
Vue.use(Vuex);

export const TODO_CREATE = "TODO_CREATE";
export const TODO_TOGGLE_DONE = "TODO_TOGGLE_DONE";
export const TODO_DELETE = "TODO_DELETE";


const vuexPersist = new VuexPersist({
  key: 'vue-todo',
  storage: window.localStorage
});

export default new Vuex.Store({
  state: {
    todo_list: [
      {
        text: "Buy a milk",
        isDone: false
      }
    ]
  },
  mutations: {
    [TODO_CREATE](state, payload) {
      state.todo_list.push({ text: payload, isDone: false });
    },
    [TODO_TOGGLE_DONE](state, payload) {
      state.todo_list.forEach(todo => {
        if (todo === payload) {
          todo.isDone = !todo.isDone;
        }
      });
    },
    [TODO_DELETE](state, payload) {
      state.todo_list = state.todo_list.filter(todo => todo !== payload);
    }
  },
  actions: {},
  modules: {},
  plugins: [vuexPersist.plugin]
});
