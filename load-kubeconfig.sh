HOST=$1

scp root@${HOST}:/etc/rancher/k3s/k3s.yaml ./
sed -i 's/127.0.0.1/${HOST}/' k3s.yaml

kubectl create secret generic regcred \
    --from-file=.dockerconfigjson=~/.docker/config.json \
    --type=kubernetes.io/dockerconfigjson