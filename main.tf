terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "2.3.0"
    }
  }
}

variable "do_token" {
  type = string
}
variable "public_key" {
  type = string
}

# Configure the DigitalOcean Provider
provider "digitalocean" {
  token = var.do_token
}

resource "digitalocean_ssh_key" "tf-default" {
  name       = "Terraform Default"
  public_key = var.public_key
}

resource "digitalocean_domain" "default" {
  name = "operatoronline.ru"
}

resource "digitalocean_record" "A-wildcard" {
  domain = digitalocean_domain.default.name
  type   = "A"
  name   = "*.wsr"
  value  = digitalocean_droplet.wsr-server.ipv4_address
}

resource "digitalocean_record" "A-wsr" {
  domain = digitalocean_domain.default.name
  type   = "A"
  name   = "wsr"
  value  = digitalocean_droplet.wsr-server.ipv4_address
}

# Create a web server
resource "digitalocean_droplet" "wsr-server" {
  image              = "ubuntu-20-04-x64"
  name               = "wsr-server"
  region             = "ams3"
  private_networking = true
  size               = "s-1vcpu-1gb"
  ssh_keys           = [digitalocean_ssh_key.tf-default.fingerprint]
  user_data          = <<EOT
#!/bin/bash
mkdir /etc/rancher/
mkdir /etc/rancher/k3s
export PUBLIC_IPV4=$(curl -s http://169.254.169.254/metadata/v1/interfaces/public/0/ipv4/address)
echo 'tls-san:
    - "'$PUBLIC_IPV4'"' > /etc/rancher/k3s/config.yaml
curl -sfL https://get.k3s.io | sh -
    EOT
}

output "cluster_ip_addr" {
  value = digitalocean_droplet.wsr-server.ipv4_address
}


# mkdir /etc/rancher/k3s
# IP=$(curl https://ipecho.net/plain)
# echo '
# tls-san:
#   - "'$IP'"
# ' > /etc/rancher/k3s/config.yaml
